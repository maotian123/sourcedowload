# Install script for directory: /home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/GeographicLib" TYPE FILE FILES
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Accumulator.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/AlbersEqualArea.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/AzimuthalEquidistant.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/CassiniSoldner.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/CircularEngine.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Constants.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/DMS.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Ellipsoid.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/EllipticFunction.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/GARS.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/GeoCoords.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Geocentric.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Geodesic.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/GeodesicExact.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/GeodesicLine.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/GeodesicLineExact.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Geohash.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Geoid.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Georef.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Gnomonic.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/GravityCircle.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/GravityModel.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/LambertConformalConic.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/LocalCartesian.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/MGRS.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/MagneticCircle.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/MagneticModel.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Math.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/NearestNeighbor.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/NormalGravity.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/OSGB.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/PolarStereographic.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/PolygonArea.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Rhumb.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/SphericalEngine.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/SphericalHarmonic.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/SphericalHarmonic1.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/SphericalHarmonic2.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/TransverseMercator.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/TransverseMercatorExact.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/UTMUPS.hpp"
    "/home/maotian/soft/sourcedowload/GeographicLib-1.52/include/GeographicLib/Utility.hpp"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/GeographicLib" TYPE FILE FILES "/home/maotian/soft/sourcedowload/GeographicLib-1.52/build/include/GeographicLib/Config.h")
endif()

